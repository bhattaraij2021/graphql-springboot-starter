# Graphql SpringBoot Prototype 

# Project setup

## DB configuration
 - For the application to run properly, you have to create a Vehicle table in the local database and create a sequence.
 - DB configuration need to be added in the application properties file for 
 
     a) spring.datasource.password
     
     b) spring.datasource.username
     
     c) spring.datasource.url

## Running the application
- From Intellij Run configuration with following configuration.
    
    a) Main class: com.jacobs.innovation.DemoApplication
    
    b) Working directory: path to the demo project in your vm
    
    c) Use classpath of module: demo
    
    d) JRE: 11 - SDK of 'demo module'
    
    e) Maven goal:  -Dskiptests clean install    
    
- From commandline: java -jar ./target/demo-0.0.1-SNAPSHOT.jar ./src/main/java/com/jacobs/innovation/DemoApplication.java
###  mvn spring-boot:run
This service runs on port 8080.

The service looks for the database connection from DB configuration spring.datasource.url default for right now is at localhost:1521:oscar (oracle database).

## Using the application
### Set up graphiql console
- In the web browser, enter the URL **http://localhost:8080/login  
- localhost:8080/login page with username "user" and password will be printed in the server console which changes in every restart of the application for example in the following format: 

  Using generated security password: 19250b70-3e69-4da2-b9dc-998e5d34c603
- Now access the GraphQL console by entering the URL **http://localhost:8081/graphiql**.

### Available services
The available queries and mutation services are as follows:

createVehicle, getVehicle and getVehicle

### Required Mutation and Query variables for the services Examples are below
a) createVehicle

Mutation: 

mutation newCreateVehicle ($newVehicle: VehicleInput){
  createVehicle(input: $newVehicle){
    id
    type
    modelCode
    brandName
    launchDate
  }
}


Query variable:


{
  "newVehicle": {
    "type": "MID-SUV",
    "modelCode": "TRAX",
    "brandName": "CHEVROLET",
    "launchDate": "2021-01-01"
  }
}

b) getVehicle

Query:


query{
  getVehicle(id: 1041){
    modelCode
    id
    brandName
    launchDate
  }
}

c) getVehicles

Query:


query{
  getVehicles(count: 7){
    modelCode
    id
    type
    brandName
  }
}
package com.jacobs.innovation;

import com.graphql.spring.boot.test.*;
import com.jacobs.innovation.dao.entity.VehicleDTO;
import com.jacobs.innovation.mutation.input.VehicleInput;
import com.jacobs.innovation.service.VehicleService;
import io.micrometer.core.instrument.util.IOUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;

/**
 * VehicleMockUnitTests class handles the unit test prototype for VehicleService graphql services
 *
 * author jbhattarai
 * date 04/08/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DemoApplication.class)
public class VehicleMockUnitTests {

    private static final String GRAPHQL_QUERY_REQUEST_PATH = "graphql/request/%s.graphql";
    private static final String GRAPHQL_QUERY_RESPONSE_PATH = "graphql/response/%s.json";

    @Autowired
    private GraphQLTestTemplate graphQLTestTemplate;

    @MockBean
    VehicleService vehicleServiceMock;

    static VehicleDTO vehicle= new VehicleDTO();

    /**
     * method which handles the creation of VehicleDTO object before running the test methods
     */
    @BeforeAll
    static void setUp() {
        vehicle.setId(2021);
        vehicle.setBrandName("BMW");
        vehicle.setModelCode("X6");
        vehicle.setType("SUV");
        vehicle.setLaunchDate(new Date());
    }

    /**
     * method is a prototype of mock unit tests the createVehicle method
     * injects vehicleDTO object "vehicle" when createVehicle requested from the
     * request.crerate-vehicle.graphql file
     * test compares the response with expected response from response.create-vehicle.json
     * @throws IOException
     */
    @Test
    public void createVehicleUnitTest() throws IOException {
        var testName = "create-vehicle";
        VehicleInput vehicleInput = new VehicleInput();
        vehicleInput.setBrandName("BMW");
        vehicleInput.setModelCode("X6");
        vehicleInput.setType("SUV");
        vehicleInput.setLaunchDate(null);
        vehicle.setLaunchDate(null);
        doReturn(vehicle).when(vehicleServiceMock).createVehicle(vehicleInput);
        var graphqlResponse = graphQLTestTemplate.postForResource(format(GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = read(format(GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(graphqlResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals("true", expectedResponseBody, graphqlResponse.getRawResponse().getBody());
    }
    /**
     * method is a prototype of mock unit tests the getVehicles method
     * injects List<VehicleDTO> object "vehicleDTOS" when getVehicles requested from the
     * request.get-vehicles.graphql file
     * test compares the response with expected response from response.get-vehicles.json
     * @throws IOException
     */
    @Test
    public void getVehiclesUnitTest() throws IOException {
        List<VehicleDTO> vehicleDTOS = new ArrayList<>();
        vehicleDTOS.add(vehicle);
        var testName = "get-vehicles";
        doReturn(vehicleDTOS).when(vehicleServiceMock).getAllVehicles(1);
        var graphqlResponse = graphQLTestTemplate.postForResource(format(GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = read(format(GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(graphqlResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals("true", expectedResponseBody, graphqlResponse.getRawResponse().getBody());
    }
    /**
     * method is a prototype of mock unit tests the getVehicle method
     * injects VehicleDTO object "vehicle" when getVehicle requested from the
     * request.get-vehicle.graphql file
     * test compares the response with expected response from response.get-vehicle.js
     * @throws IOException
     */

    @Test
    public void getVehicleUnitTest() throws IOException{
        var testName = "get-vehicle";
        doReturn(Optional.of(vehicle)).when(vehicleServiceMock).getVehicle(1001);
        var graphqlResponse = graphQLTestTemplate.postForResource(format(GRAPHQL_QUERY_REQUEST_PATH, testName));
        var expectedResponseBody = read(format(GRAPHQL_QUERY_RESPONSE_PATH, testName));
        assertThat(graphqlResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals("true", expectedResponseBody, graphqlResponse.getRawResponse().getBody());
    }
    /**
     * method reads the file for given resource path
     *
     * @throws IOException
     */

    private String read(String location) throws IOException {
        return IOUtils
                .toString(new ClassPathResource(location).getInputStream(), StandardCharsets.UTF_8);
    }
}
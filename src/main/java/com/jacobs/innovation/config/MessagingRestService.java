package com.jacobs.innovation.config;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RequestMapping("/api")
@RestController
public class MessagingRestService {

    @GetMapping("/")
    public String whoami(@AuthenticationPrincipal Jwt jwt) {
        return String.format("Hello, %s", jwt.getClaim("preferred_username").toString());
    }

    @GetMapping(path = "/authenticate")
    public String getPublicResource(HttpServletRequest httpServletRequest) {
        return "Hello, you are now authenticated to use any other resources" + new Date().toString();
    }

    @GetMapping("/user")
    public String protectedHello() {
        return "Hello World, i am a user with user scope";
    }

    @GetMapping("/admin")
    public String admin() {
        return "Hello World from admin";
    }

}

package com.jacobs.innovation.service;

import com.jacobs.innovation.dao.entity.Vehicle;
import com.jacobs.innovation.dao.entity.VehicleDTO;
import com.jacobs.innovation.dao.repository.VehicleRepository;
import com.jacobs.innovation.mutation.input.VehicleInput;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.System.*;

@Service
public class VehicleService {
    private static final Logger logger = LoggerFactory.getLogger(VehicleService.class);
    public VehicleService() {  }

    @Autowired
    private VehicleRepository vehicleRepository;

    public VehicleService(final VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Transactional
    public VehicleDTO createVehicle(final VehicleInput vehicleInput){
        final Vehicle vehicle= new Vehicle(vehicleInput.getType(), vehicleInput.getModelCode(), vehicleInput.getBrandName(), vehicleInput.getLaunchDate());
        Vehicle savedVehicle=  vehicleRepository.save(vehicle);
        logger.info(String.format("Following Vehicles are retrieved from the DB:      %s", savedVehicle));
        return convertToVehicleDTO(savedVehicle);
    }

    @Transactional(readOnly = true)
    public List<VehicleDTO> getAllVehicles(final int count) {
        List<VehicleDTO> vehicleDTOS;
        List<Vehicle> vehicleList = vehicleRepository.findAll().stream().limit(count).collect(Collectors.toList());
        vehicleDTOS = vehicleList.stream().map(this::convertToVehicleDTO).collect(Collectors.toList());
        logger.info(String.format("Following Vehicles are retrieved from the DB:      %s", vehicleDTOS));
        return vehicleDTOS;
    }

    @Transactional(readOnly = true)
    public Optional<VehicleDTO> getVehicle(final int id) {
        VehicleDTO vehicleDTO;
        Vehicle vehicle;
        boolean value = vehicleRepository.findById(id).isPresent();
        if (!value) { return Optional.empty();
        } else {
            vehicle = vehicleRepository.findById(id).get();
            vehicleDTO = convertToVehicleDTO(vehicle);
            logger.info(String.format("Following Vehicles are retrieved from the DB:      %s", vehicleDTO));
        }

        return Optional.of(vehicleDTO);
    }

    @NotNull
    private VehicleDTO convertToVehicleDTO(Vehicle savedVehicle) {
        VehicleDTO vehicleDTO = new VehicleDTO();
        vehicleDTO.setId(savedVehicle.getId());
        vehicleDTO.setBrandName(savedVehicle.getBrandName());
        vehicleDTO.setLaunchDate(savedVehicle.getLaunchDate());
        vehicleDTO.setModelCode(savedVehicle.getModelCode());
        vehicleDTO.setType(savedVehicle.getType());
        return vehicleDTO;
    }
}

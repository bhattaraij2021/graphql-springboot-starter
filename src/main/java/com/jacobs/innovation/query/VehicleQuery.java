package com.jacobs.innovation.query;

import com.jacobs.innovation.dao.entity.VehicleDTO;
import com.jacobs.innovation.service.VehicleService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;

@Component
@CrossOrigin
public class VehicleQuery implements GraphQLQueryResolver {
    @Autowired
    private VehicleService vehicleService;


    public List<VehicleDTO> getVehicles(final int count){
        return this.vehicleService.getAllVehicles(count);
    }


    public Optional<VehicleDTO> getVehicle(final int id){
        return this.vehicleService.getVehicle(id);
    }
}

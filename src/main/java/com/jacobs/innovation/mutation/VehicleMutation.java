package com.jacobs.innovation.mutation;

import com.jacobs.innovation.dao.entity.VehicleDTO;
import com.jacobs.innovation.mutation.input.VehicleInput;
import com.jacobs.innovation.service.VehicleService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import static java.lang.System.*;

@Component
@CrossOrigin
public class VehicleMutation implements GraphQLMutationResolver {
    public VehicleMutation( ) {    }

    @Autowired
    private VehicleService vehicleService;


    public VehicleDTO createVehicle(final VehicleInput vehicleInput) {
        return this.vehicleService.createVehicle(vehicleInput);

//        return this.vehicleService.createVehicle(vehicleInput.getType(), vehicleInput.getModelCode(), vehicleInput.getBrandName(), vehicleInput.getLaunchDate());
    }
}

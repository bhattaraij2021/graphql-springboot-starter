package com.jacobs.innovation.mutation.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleInput implements Serializable {
    private static final long serialVersionUID = -3880302340069503145L;

    private String type;
    private String modelCode;
    private String brandName;
    private Date launchDate= new Date();
}

package com.jacobs.innovation.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.xml.validation.Schema;
import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode
@javax.persistence.Entity
@Table(name = "vehicle", schema = "graphql")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    public Vehicle(String type, String modelCode, String brandName, Date launchDate) {
        this.type = type;
        this.modelCode = modelCode;
        this.brandName = brandName;
        this.launchDate = launchDate;
    }

    public Vehicle() {
    }

    @Id
    @GeneratedValue(generator = "vehicle_id", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name="vehicle_id", schema = "graphql",sequenceName="vehicle_id_seq", allocationSize=1)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;

    @Column(name = "type")
    private String type;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "launchdate")
    private Date launchDate= new Date();

}

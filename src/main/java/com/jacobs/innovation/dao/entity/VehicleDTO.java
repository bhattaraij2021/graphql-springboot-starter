package com.jacobs.innovation.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode
public class VehicleDTO {

    private int id;
    private String type;
    private String modelCode;
    private String brandName;
    private Date launchDate= new Date();
}
